# Flask API & Pyomo + CPLEX

Ejecutar app.py activa el server Flask que responde a POST Requests en 0.0.0.0:8085/op1.

La request debe incluir 4 csv:

```
Nodos: Con columnas 'Node', + opcionales
Arcos: Con columnas 'StartNode', 'EndNode', 'Capacity', 'Attackable', + opcionales
Nodos-Mercancías: Con columnas 'Node', 'Commodity', 'SupplyDemand', + opcionales
Arcos-Mercancías: Con columnas 'StartNode', 'EndNode', 'Commodity', 'Cost', 'Capacity', + opcionales

Donde 'Node', StartNode' y 'EndNode' refieren a los nombres de los mismos.
```

Que son pasados a multi_commodity_flow_interdict.py, inicializa las estructuras de datos de Pyomo,
ejecuta el solver CPLEX y retorna una respuesta HTTP con un JSON de resultado en formato:

```
{
    "flow":[
        {"commodity":"Corn","node1":"B","node2":"End","quantity":10},
        {"commodity":"Corn","node1":"C","node2":"B","quantity":10},
        ...
    ],
    "interdicts":[
        {"node1":"B","node2":"D"},
        {"node1":"C","node2":"D"},
        ...
    ],
    "remaining_demand":[
        {"commodity":"Rice","demand":20,"node":"D"},
        ...
    ],
    "remaining_supply":[
        {"commodity":"Rice","node":"Start","supply":20},
        ...
    ],
    "total":143000
}
```

## Conda Enviroment Setup

```
conda create --name <nombreEntorno>
conda install -c conda-forge pyomo
conda install -c conda-forge pyomo.extras
conda install -c conda-forge glpk
conda install -c conda-forge flask
conda install -c conda-forge flask-cors
conda install -c conda-forge waitress
```

## Servicios y ejecución en el servidor

Para correr como servicio una aplicación de python en un entorno determinado se debe generar un archivo .bat en el mismo directorio que la aplicación a ejecutar:

```
@echo on
CALL C:\<path>\Anaconda3\Scripts\activate.bat <nombreEntorno>
python app.py
pause
```

Donde < path > refiere a la raíz de la instalación de Anaconda y < nombreEntorno > al entorno a utilizar.

La gestión de servicios puede realizarse con NSSM (https://nssm.cc/):

```
nssm install <nombreServicio>
```

Abre una UI que permite configurar todos los parametros, mas información en la documentación. (https://nssm.cc/usage)

Los servicios pueden gestionarse vía services.msc (Win + R -> services.msc)

## Main Thread Error - Signal Handling

Referencia para solución de errores a la hora de correr modelos de Pyomo ejecutados por un servidor Flask:

https://github.com/PyUtilib/pyutilib/issues/31

## Build & Deploy at INGAR

- Login at Server windows (conda env prod1Env)
```
- cd c:\code\app-lab3-pyomo
- git pull origin master
```
- Kill if running, remove logs, copy new version, and restart
```
copy C:\code\app-lab3-pyomo\*.py C:\apps\app-lab2-py\
copy C:\code\app-lab3-pyomo\start-app-lab3-py.bat C:\apps\app-lab3-py\
cd C:\apps\app-lab2-py
C:\apps\app-lab3-py\start-app-lab3-py.bat
```
- del C:\apps\app-lab3-py\app-lab3.log

### view live logs en powershell
```
view live logs en powershell: Get-Content -Path "C:\apps\app-lab3-py\app-lab3.log" -Wait
```
