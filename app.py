from flask import Flask
from flask import request, jsonify
from flask_cors import CORS
from waitress import serve
from multi_commodity_flow_interdict import get_results
import time

app = Flask(__name__)
app.use_reloader= True
app.debug= False
CORS(app)


@app.route('/op1', methods=['POST'])
def postJsonHandler():
    start = time.strftime("%c")
    print("Executing optimizer: " + start)

    fileArcs = request.files['file1']
    fileNodes = request.files['file2']
    fileNodeCom = request.files['file3']
    fileArcCom = request.files['file4']
    attacks = request.form['attacks']

    result = get_results(fileNodes, fileNodeCom, fileArcs, fileArcCom, int(attacks))
    resp = jsonify(result)

    resp.status_code = 200
    return resp


if __name__ == "__main__":
    print("Starting app at " + time.strftime("%c"))
    #serve(app, host='0.0.0.0', port=8085)
    serve(app, listen = '*:8085')
